import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux';
import * as headerStyles from '../styles/header.scss';

import {SORT_BY_RATING} from "../constants/action-types";

export class HeaderComponent extends Component {



    render() {
        return (
            <header className={'header'}>
                <Link className={'header-categories'} to='/flowers'>
                    <span>Flowers</span>
                </Link>
                <Link className={'header-categories'} to='/cars'>
                    <span>Cars</span>
                </Link>
            </header>
        );
    }
}
