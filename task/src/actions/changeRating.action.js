import {
    INCREASE_RATING,
    DECREASE_RATING,
} from "../constants/action-types";


export function increaseRatingAction(event, id) {
    return (dispatch, getState) => {
        dispatch({
            type: INCREASE_RATING,
            data: getState().photosList.list,
            event,
            id
        })
    };
}

export function decreaseRatingAction(event, id) {
    return (dispatch, getState) => {
        dispatch({
            type: DECREASE_RATING,
            data: getState().photosList.list,
            event,
            id
        })
    }
}