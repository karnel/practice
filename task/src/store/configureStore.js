import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers/app.reducer';

const initialState = {
    photosList: []
};

const enhancer = compose(
    applyMiddleware(thunk),
    (typeof window !== 'undefined' && window.devToolsExtension)
        ? window.devToolsExtension()
        : f => f
);

export default function configureStore() {
    return createStore(
        rootReducer,
        initialState,
        enhancer,
    );
}