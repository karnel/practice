import React from 'react';
import {ContentContainer} from "./content.container";
import {HeaderContainer} from "./header.container";

export const AppContainer = () => (
    <div>
        <HeaderContainer />
        <ContentContainer />
    </div>
)